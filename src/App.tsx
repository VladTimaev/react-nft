import React from 'react';
import "./index.css";

import Header from "./components/Header";
import Footer from "./components/Footer";
import Banner from "./components/Banner";
import FooterContainer from "./components/FooterContainer";


export default function App() {
  return (
    <div className="App">
        <Header />
        <Banner />
        <FooterContainer />
        <Footer />
    </div>
  );
}


