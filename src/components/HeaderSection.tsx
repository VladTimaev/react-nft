import React, { MouseEvent } from "react";

function Form() {
    function handleSumbit (event: MouseEvent<HTMLFormElement>) {
        event.preventDefault()
    }

    return (
        <form onSubmit={handleSumbit} method="get" id="revue-form" name="revue-form" className="flex max-w-xl md:mx-auto">
            <div className="w-full">
                <div className="relative h-full">
                    <input type="email" id="member_email" name="member[email]"
                           className="block w-full px-3 py-4 pl-10 text-base text-gray-900 bg-white rounded-l-xl revue-form-field dark:placeholder-gray-400 dark:text-white "
                           placeholder="Email Address" />
                </div>
            </div>
            <div className="revue-form-actions">
                <input type="submit" name="member[subscribe]" id="member_submit"
                       className="w-full px-4 py-4 text-base font-semibold text-center text-black bg-white cursor-pointer rounded-r-xl formkit-submit hover:opacity-75 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-blue-600"
                       value="Subscribe" />
            </div>
        </form>
    );
}


export default function HeaderSection () {
    return (
        <>
            <aside className="py-16 lg:py-20 text-white opacity-75">
                <div className="px-4 mx-auto max-w-8xl">
                    <h2 className="mb-4 text-4xl font-bold tracking-tight md:font-extrabold lg:leading-none dark:text-white md:text-center lg:mb-7">Never Miss a drop!</h2>
                        <p className="mb-6 text-base md:mb-10 md:text-center md:text-xl lg:px-20 xl:px-56 font-light">Subscribe to our ultra-exclusive drop list and be the first to know about upcoming dolby drops.</p>
                    <div id="revue-embed" className="mb-4">
                        <Form />
                    </div>
                </div>
            </aside>
        </>
    )
}