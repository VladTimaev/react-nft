import React from "react";
import {products} from "../products/products";
import HeaderSection from "./HeaderSection";


export default function Nft () {

    return (
        <div className="rounded overflow-hidden">
            <div className="mx-auto max-w-2xl py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8 text-white opacity-75">
                <h2 className="text-2xl font-bold tracking-tight">Features Artworks</h2>
                <div className="flex items-center justify-end">
                    <h2 className="mt-3 text-base sm:mx-auto sm:mt-5 sm:max-w-xl sm:text-lg md:mt-5 md:text-xl lg:mx-0">
                        Let’s get started
                    </h2>
                    <img
                        className="mt-5 ml-3.5"
                        src="/assets/icons/arrow.png" />
                </div>
                <div className="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                    {products.map((product) => (
                        <div key={product.id} className="group relative ">
                            <div className="min-h-80 aspect-w-1 aspect-h-1 w-full overflow-hidden rounded-t group-hover:opacity-100 lg:aspect-none lg:h-80">
                                <img
                                    src={product.imageSrc}
                                    alt={product.imageAlt}
                                    className="h-full w-full object-cover object-center lg:h-full lg:w-full"
                                />
                            </div>
                            <div className="card">
                                   <div className="row-auto justify-between p-2">
                                       <h3 className="font-bold pb-3">{product.name}</h3>
                                       <p className="pb-3">{product.title}</p>
                                      <div className="flex items-center justify-between pb-3">
                                          <h3 className="text-sm">
                                              <a href={product.href}>
                                                  <span aria-hidden="true" className="text-sm font-medium">{product.nft}</span>
                                              </a>
                                          </h3>
                                         <div className="flex">
                                             <img className="pr-1.5 mb-1" src='/assets/icons/img.png' />
                                             <p className="text-sm font-bold">{product.price}</p>
                                         </div>
                                      </div>
                                      <div className="flex items-center justify-between">
                                          <img src='/assets/icons/heart.png' />
                                          <button className="bg-yellow-300 hover:bg-yellow-400 text-black font-bold py-2 px-4 rounded">
                                              BUY
                                          </button>
                                      </div>
                                   </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            <HeaderSection />
        </div>
    )
}