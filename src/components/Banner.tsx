import React from "react";


export default function Banner () {
    return (
        <>
                <div className="relative overflow-hidden text-white opacity-75 footer_container">
                    <div className="mx-auto max-w-7xl">

                            <img className="float-right border_blockchain" src="/assets/images/blockchain.png" alt="blockchain" />
                            <img className="float-right border_bank" src="/assets/images/bank.png" alt="bank" />
                            <img className="border_boy" src="/assets/images/boy.png" alt="boy" />

                        <div className="relative z-10 pb-8 sm:pb-16 md:pb-20 lg:w-full lg:max-w-2xl lg:pb-28 xl:pb-32">
                            <main className="mx-auto mt-10 max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
                                <div className="sm:text-center lg:text-left">
                                    <h1 className="text-6xl font-bold tracking-tight sm:text-5xl md:text-6xl">
                                        <span className="block ">The Premier Marketplace</span>
                                        <span className="block xl:inline">for Nifties</span>
                                    </h1>
                                    <p className="mt-3 text-base sm:mx-auto sm:mt-5 sm:max-w-xl sm:text-lg md:mt-5 md:text-xl lg:mx-0">dolby is the premier marketplace for Nifties, which are digital items you can truly own. Digital Items have existed for a long time, but never like this.</p>
                                   <div className="flex items-center">
                                       <h2 className="mt-3 text-base sm:mx-auto sm:mt-5 sm:max-w-xl sm:text-lg md:mt-5 md:text-xl lg:mx-0">
                                           Let’s get started
                                       </h2>
                                       <img
                                           className="mt-5 ml-3.5"
                                           src="/assets/icons/arrow.png" />
                                   </div>
                                </div>
                            </main>
                        </div>
                    </div>
                </div>
        </>
    )
}
