import React from "react";

const products = [
    {
        id: 1,
        nft: 'Price',
        name: 'Jenuine Ruddy Girl',
        title: 'Keeping yourself safe when buying NFTs on dolbi',
        href: '#',
        imageSrc: '/assets/images/nft/nft-7455.png',
        imageAlt: "nft",
        price: '€25.00',
    },
    {
        id: 2,
        nft: 'Price',
        name: 'Jenuine Ruddy Girl',
        title: 'Keeping yourself safe when buying NFTs on dolbi',
        href: '#',
        imageSrc: '/assets/images/nft/nft-7456.png',
        imageAlt: "nft",
        price: '€25.00',
    },
    {
        id: 3,
        nft: 'Price',
        name: 'Jenuine Ruddy Girl',
        title: 'Keeping yourself safe when buying NFTs on dolbi',
        href: '#',
        imageSrc: '/assets/images/nft/nft-7458.png',
        imageAlt: "nft",
        price: '€25.00',
    }
]


export default function FooterContainer () {

    return (
        <div className="rounded overflow-hidden">
            <div className="mx-auto max-w-2xl py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8 text-white opacity-75">
                <h2 className="text-2xl font-bold tracking-tight">Recent Activity</h2>
                <div className="flex items-center justify-end">
                    <h2 className="mt-3 text-base sm:mx-auto sm:mt-5 sm:max-w-xl sm:text-lg md:mt-5 md:text-xl lg:mx-0">
                        Browse More
                    </h2>
                    <img
                        className="mt-5 ml-3.5"
                        src="/assets/icons/arrow.png" />
                </div>
                <div className="mt-6 grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8 flex justify-center">
                    {products.map((product) => (
                        <div key={product.id} className="group relative">
                            <div className="min-h-80 aspect-w-1 aspect-h-1 w-full overflow-hidden rounded-t group-hover:opacity-100 lg:aspect-none lg:h-80">
                                <img
                                    src={product.imageSrc}
                                    alt={product.imageAlt}
                                    className="h-full w-full object-cover object-center lg:h-full lg:w-full"
                                />
                            </div>
                            <div>
                                <div className="row-auto justify-between p-2">
                                    <h3 className="font-bold pb-3">{product.name}</h3>
                                    <p className="pb-3">{product.title}</p>
                                    <div className="flex items-center justify-between pb-3">
                                        <div className="flex">
                                            <img className="pr-1.5 mb-1" src='/assets/icons/img.png' />
                                            <p className="text-sm font-bold">{product.price}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}