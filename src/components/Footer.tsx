import React from 'react';

export default function Footer() {
    return (
        <>
            <hr />
            <footer className="dark:bg-gray-900 mx-auto max-w-7xl">
                <div className="flex justify-between py-8 px-8">
                        <div className="flex text-white font-bold">
                            <div className="p-2 items-center">
                                <h2 className=" mb-3.5 pl-1">Join the community</h2>
                                <button className="bg-blue-500 hover:bg-blue-700 rounded p-2.5 m-1.5 btn_social">
                                    <img src="/assets/icons/social/twitter.png" alt="twitter" />
                                </button>
                                <button className="bg-blue-500 hover:bg-blue-700 rounded p-2.5 m-1.5 btn_social">
                                    <img src="/assets/icons/social/instagram.png" alt="instagram" />
                                </button>
                                <button className="bg-blue-500 hover:bg-blue-700 rounded p-2.5 m-1.5 btn_social">
                                    <img src="/assets/icons/social/discord.png" alt="discord" />
                                </button>
                                <button className="bg-blue-500 hover:bg-blue-700 rounded p-2.5 m-1.5 btn_social">
                                    <img src="/assets/icons/social/platform.png" alt="platform" />
                                </button>
                                <button className="bg-blue-500 hover:bg-blue-700 rounded p-2.5 m-1.5 btn_social">
                                    <img src="/assets/icons/social/email.png" alt="email" />
                                </button>
                            </div>
                          </div>
                    <div className="columns-2
                               lg:grid-cols-4
                               md:grid-cols-2
                               text-white
                               mx-auto">
                        <ul>
                            <li className="mb-6 text-sm">
                                <a href="#" className="hover:underline font-semibold">Menu</a>
                            </li>
                            <li className="mb-4">
                                <a href="#" className="hover:underline">Explore</a>
                            </li>
                            <li className="mb-4">
                                <a href="#" className="hover:underline">My items</a>
                            </li>
                            <li className="mb-4">
                                <a href="#" className="hover:underline">Create</a>
                            </li>
                        </ul>
                        <ul>
                            <li className="mb-6 text-sm">
                                <a href="#" className="hover:underline font-semibold">Info</a>
                            </li>
                            <li className="mb-4">
                                <a href="#" className="hover:underline">Privacy Policy</a>
                            </li>
                            <li className="mb-4">
                                <a href="#" className="hover:underline">Terms of service</a>
                            </li>
                            <li className="mb-4">
                                <a href="#" className="hover:underline">Help!</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        </>
    );
}

