import React from 'react';
import Nft from "./Nft";

export default function Header() {
    return (
        <div>
            <div className="relative overflow-hidden text-white opacity-75 ">
                <div className="mx-auto max-w-7xl">
                    <div className="relative z-10 pb-8 sm:pb-16 md:pb-20 lg:w-full lg:pb-28 xl:pb-32">
                            <div className="relative px-4 pt-6 sm:px-6 lg:px-8">
                                <nav className="relative flex items-center justify-between sm:h-10 lg:justify-start" aria-label="Global">
                                    <div className="flex flex-shrink-0 flex-grow items-center lg:flex-grow-0">
                                        <div className="flex w-full items-center justify-between md:w-auto">
                                            <a href="#">
                                                <img alt="logo" className="h-8 w-auto sm:h-10" src="/assets/logo.png" />
                                            </a>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        <main className="mx-auto mt-10 max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
                            <div className="lg:text-left">
                                <div className="lg:inset-y-0 ">
                                    <img className="float-right h-56 sm:h-72 md:h-96 banner" src="/assets/banner.png" alt="banner" />
                                </div>
                                <p className="mt-3 sm:mt-5 sm:max-w-xl sm:text-lg md:mt-5 md:text-xl lg:mx-0">Featured Film</p>
                                <h1 className="text-4xl font-bold tracking-tight sm:text-5xl md:text-6xl">
                                    <span className="block">ZERO</span>
                                    <span className="block xl:inline">CONTACT</span>
                                </h1>
                                <p className="mt-3 sm:mt-5 sm:max-w-xl sm:text-lg md:mt-5 md:text-xl lg:mx-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit gravida enim, orci morbi elementum gravida.</p>
                                <div className="flex items-center">
                                    <h2 className="mt-3 sm:mt-5 sm:max-w-xl sm:text-lg md:mt-5 md:text-xl lg:mx-0">
                                        Let’s get started
                                    </h2>
                                    <img
                                        className="mt-5 ml-3.5"
                                        src="/assets/icons/arrow.png" />
                                </div>
                            </div>
                            <div className="hidden md:block md:space-x-8 md:pr-4 mt-12">
                               <ul className="flex">
                                   <li>
                                       <a href="#" className="font-medium hover:text-blue-400">2k | Active user</a>
                                   </li>
                                   <li className="ml-6">
                                       <a href="#" className="font-medium hover:text-blue-400">5k | Music Video</a>
                                   </li>
                                   <li className="ml-6">
                                       <a href="#" className="font-medium hover:text-blue-400">7k | Visual Art</a>
                                   </li>
                               </ul>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
            <Nft />
        </div>
    );
}
